const express = require("express");
const User = require("../models/user");

const router = express.Router();

// Маршрут для отримання всіх користувачів
router.get("/users", async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Маршрут для отримання користувача за його ID
router.get("/users/:id", async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            return res.status(404).send("User not found");
        }
        res.json(user);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Маршрут для створення нового користувача
router.post("/users", async (req, res) => {
    try {
        const user = new User(req.body);
        await user.save();
        res.status(201).json(user);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрут для оновлення користувача за його ID
router.put("/users/:id", async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!user) {
            return res.status(404).send("User not found");
        }
        res.json(user);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрут для видалення користувача за його ID
router.delete("/users/:id", async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        if (!user) {
            return res.status(404).send("User not found");
        }
        res.status(200).json(user);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрут для видалення всіх користувачів
router.delete("/users", async (req, res) => {
    try {
        const result = await User.deleteMany();
        res.status(200).json(result);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

module.exports = router;
