const mongoose = require("mongoose");
const validator = require("validator");

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: {
            validator: validator.isEmail,
            message: "Email is invalid"
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
        trim: true,
        validate: {
            validator: value => {
                return !value.toLowerCase().includes("password");
            },
            message: "Password cannot contain the word 'password'"
        }
    },
    age: {
        type: Number,
        default: 0,
        validate: {
            validator: value => {
                return value === null || value >= 0;
            },
            message: "Age must be a positive number"
        }
    }
});

const User = mongoose.model("User", userSchema);

module.exports = User;
