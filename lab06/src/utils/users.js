const users = [] // Масив для зберігання користувачів у чаті

// Функція для додавання користувача до масиву users
const addUser = ({ id, username, room }) => {
    username = username.trim().toLowerCase() // Перетворення імені користувача на малий регістр та видалення зайвих пробілів
    room = room.trim().toLowerCase() // Перетворення назви кімнати на малий регістр та видалення зайвих пробілів

    // Перевірка наявності імені користувача та назви кімнати
    if (!username || !room) {
        return {
            error: 'Username and room are required!'
        }
    }

    // Перевірка, чи ім'я користувача не зайняте в обраній кімнаті
    const existingUser = users.find((user) => {
        return user.room === room && user.username === username
    })
    if (existingUser) {
        return {
            error: 'Username is in use!'
        }
    }

    const user = { id, username, room } // Створення об'єкта користувача
    users.push(user) // Додавання користувача до масиву users
    return { user } // Повертаємо об'єкт користувача без помилки
}

// Функція для видалення користувача з масиву users за його id
const removeUser = (id) => {
    const index = users.findIndex((user) => user.id === id) // Знаходимо індекс користувача в масиві за його id

    if (index !== -1) {
        return users.splice(index, 1)[0] // Видаляємо користувача з масиву та повертаємо його об'єкт
    }
}

// Функція для отримання користувача з масиву users за його id
const getUser = (id) => {
    return users.find((user) => user.id === id) // Знаходимо користувача в масиві за його id і повертаємо його
}

// Функція для отримання користувачів в певній кімнаті з масиву users
const getUsersInRoom = (room) => {
    room = room.trim().toLowerCase() // Перетворення назви кімнати на малий регістр та видалення зайвих пробілів
    return users.filter((user) => user.room === room) // Фільтруємо користувачів за кімнатою та повертаємо список користувачів
}

// Функція для отримання користувача з масиву users за його іменем
const getUserByUsername = (username) => {
    username = username.trim().toLowerCase() // Перетворення імені користувача на малий регістр та видалення зайвих пробілів
    return users.find((user) => user.username === username) // Знаходимо користувача в масиві за його ім'ям і повертаємо його
}

// Експорт функцій для використання у інших файлах
module.exports = {
    addUser,
    removeUser,
    getUser,
    getUsersInRoom,
    getUserByUsername
}
