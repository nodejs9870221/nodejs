const generateMessage = (username, text) => {
    return {
        username, // Ім'я користувача
        text, // Текст повідомлення
        createdAt: new Date().getTime() // Час створення повідомлення
    }
}

const generateLocationMessage = (username, url) => {
    return {
        username, // Ім'я користувача
        url, // URL локації
        createdAt: new Date().getTime() // Час створення локаційного повідомлення
    }
}

// Експорт функцій для використання у інших файлах
module.exports = {
    generateMessage,
    generateLocationMessage
}
