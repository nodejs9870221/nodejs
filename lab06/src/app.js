// Підключення необхідних модулів та налаштування констант
const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const DIR = __dirname.replace("\\src", "");

// Імпорт функцій для роботи з користувачами та повідомленнями
const { addUser, getUsersInRoom, getUser, removeUser, getUserByUsername } = require("./utils/users");
const { generateMessage } = require("./utils/messages");

// Створення екземпляру додатку Express та налаштування статичних шляхів для веб-сторінок та стилів
const app = express();
app.use(express.static(DIR + "\\pub"));
app.use(express.static(DIR + "\\src\\styles"));

// Створення HTTP сервера за допомогою Express та підключення сокетів до сервера
const server = http.createServer(app);
const io = new Server(server);

// Налаштування маршрутів для статичних веб-сторінок
app.use(express.static(__dirname + '/../pub'));

// Обробка подій при підключенні нового користувача через сокет
io.on('connection', (socket) => {

    // Обробник події "join" для долучення користувача до чату
    socket.on('join', (options, callback) => {
        // Додавання користувача до чату та отримання інформації про помилку (якщо є) або про успішне додавання користувача
        const { error, user } = addUser({ id: socket.id, ...options })

        if (error) {
            return callback(error);
        }

        socket.join(user.room); // Додаємо користувача до кімнати за допомогою сокетів

        // Відправлення привітального повідомлення від адміністратора для нового користувача
        socket.emit('message', generateMessage('Admin', `Welcome, ${user.username}!`));

        // Відправлення повідомлення всім користувачам у кімнаті, крім нового користувача, про його приєднання
        socket.broadcast.to(user.room).emit('message', generateMessage('Admin', `${user.username} has joined!`));

        // Оновлення даних кімнати для всіх користувачів у кімнаті
        io.to(user.room).emit('roomData', {
            users: getUsersInRoom(user.room) // Отримання списку користувачів у кімнаті
        });

        callback(); // Викликаємо callback для підтвердження успішного додавання користувача
    })


    // Обробник події "sendMessage" для відправлення повідомлення в чат
    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id)

        io.to(user.room).emit('message', generateMessage(user.username, message))
        callback();
    });

    // Обробник події "disconnect" для обробки виходу користувача з чату
    socket.on('disconnect', () => {
        const user = removeUser(socket.id)

        if (user) {
            io.to(user.room).emit('message', generateMessage('Admin', `${user.username} has left!`))
            io.to(user.room).emit('roomData', {
                users: getUsersInRoom(user.room)
            })
        }
    });

    // Обробник події "userIsTyping" для відображення, що користувач пише повідомлення
    socket.on('userIsTyping', () => {
        const user = getUser(socket.id);
        if (user) {
            socket.broadcast.to(user.room).emit('userIsTyping', user.username);
        }
    });

    // Обробник події "sendPrivateMessage" для відправлення приватного повідомлення
    socket.on('sendPrivateMessage', ({ message, username }, callback) => {
        const user = getUserByUsername(username);
        const sender = getUser(socket.id);

        if (user && sender) {
            const privateMessage = generateMessage(sender.username, "private message: " + message);

            if (user.room === sender.room)
                io.to(user.id).emit('privateMessage', privateMessage);

            io.to(sender.id).emit('notification', `Your private message ${privateMessage.text} to ${user.username} has been sent`);

            callback()
        }
    });
});

// Маршрут для головної сторінки
app.get('/', (req, res) => {
    res.sendFile(DIR + "\\pub/index.html");
});

// Маршрут для сторінки чату
app.get('/chat', (req, res) => {
    res.sendFile(DIR + "\\pub/chat.html");
})

server.listen(3000, function () {
    console.log('listening on :3000');
});
