const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: {
            validator: validator.isEmail,
            message: "Email is invalid"
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
        trim: true,
        validate: {
            validator: value => {
                return !value.toLowerCase().includes("password");
            },
            message: "Password cannot contain the word 'password'"
        }
    },
    age: {
        type: Number,
        default: 0,
        validate: {
            validator: value => {
                return value === null || value >= 0;
            },
            message: "Age must be a positive number"
        }
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
});


userSchema.pre('save', async function(next) {
    const user = this;

    if (!user.isModified('password')) return next();

    try {
        const hashedPassword = await bcrypt.hash(user.password, 8);
        user.password = hashedPassword;
        next();
    } catch (error) {
        return next(error);
    }
});


userSchema.statics.findOneByCredentials = async function (email, password) {
    const user = await this.findOne({ email });

    if (!user) {
        throw new Error('User not found');
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
        throw new Error('Invalid credentials');
    }

    return user;
};

userSchema.methods.generateAuthToken = async function () {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, 'kdweueksdsjfij');
    user.tokens = user.tokens.concat({token});
    await user.save();
    return token;
};

const User = mongoose.model("User", userSchema);

module.exports = User;
