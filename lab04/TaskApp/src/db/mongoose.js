const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config();

const MONGOURL = process.env.MONGO_URL;

mongoose.connect(MONGOURL)
    .then(() => {
        console.log("Database connected successfully.");
    })
    .catch((err) => {
        console.error("Database connection error:", err);
    });
