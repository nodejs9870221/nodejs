const express = require("express");
const Task = require("../models/task");

const router = express.Router();

// Маршрутизатор для отримання всіх задач
router.get("/tasks", async (req, res) => {
    try {
        const tasks = await Task.find();
        res.json(tasks);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Маршрутизатор для отримання задачі за її ID
router.get("/tasks/:id", async (req, res) => {
    try {
        const task = await Task.findById(req.params.id);
        if (!task) {
            return res.status(404).send("Task not found");
        }
        res.json(task);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Маршрутизатор для створення нової задачі
router.post("/tasks", async (req, res) => {
    try {
        const task = new Task(req.body);
        await task.save();
        res.status(201).json(task);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрутизатор для оновлення задачі за її ID
router.put("/tasks/:id", async (req, res) => {
    try {
        const task = await Task.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!task) {
            return res.status(404).send("Task not found");
        }
        res.json(task);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрутизатор для видалення задачі за її ID
router.delete("/tasks/:id", async (req, res) => {
    try {
        const task = await Task.findByIdAndDelete(req.params.id);
        if (!task) {
            return res.status(404).send("Task not found");
        }
        res.status(200).json(task);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрутизатор для відмітки задачі як виконаної
router.patch("/tasks/:id/completed", async (req, res) => {
    try {
        const task = await Task.findByIdAndUpdate(req.params.id, { completed: true }, { new: true });
        if (!task) {
            return res.status(404).send("Task not found");
        }
        res.status(200).json(task);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

module.exports = router;