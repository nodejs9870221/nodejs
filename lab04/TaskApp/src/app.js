const express = require("express");
require("./db/mongoose");
const User = require("./models/user");
const Task = require("./models/task");
const userRouter = require("./routers/user");
const taskRouter = require("./routers/task");

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(userRouter);
app.use(taskRouter);

// Перевіряє, чи існує користувач з таким іменем
User.findOne({ name: "Alex" })
    .then(existingUser => {
        if (existingUser) {
            console.log("User already exists:", existingUser);
        } else {
            // Якщо не існує, створює нового користувача
            const newUser = new User({
                name: "Alex",
                email: "Alex@gmail.com",
                password: "0543434544",
                age: 35
            });

            // Зберігає нового користувача в базі даних
            newUser.save()
                .then(savedUser => {
                    console.log("New user saved:", savedUser);
                })
                .catch(error => {
                    console.error("Error saving user:", error);
                });
        }
    })
    .catch(error => {
        console.error("Error checking for existing user:", error);
    });

// Перевіряє, чи завдання з такою назвою вже існує
Task.findOne({ title: "do something" })
    .then(existingTask => {
        if (existingTask) {
            console.log("Task already exists:", existingTask);
        } else {
            // Якщо завдання не існує, створює нове завдання
            const newTask = new Task({
                title: "do something",
                description: "...somehow...",
                completed: false
            });

            // Зберігає нове завдання в базі даних
            newTask.save()
                .then(savedTask => {
                    console.log("New task saved:", savedTask);
                })
                .catch(error => {
                    console.error("Error saving task:", error);
                });
        }
    })
    .catch(error => {
        console.error("Error checking for existing task:", error);
    });

app.get("/", (req, res) => {
    res.send("Welcome to the server!");
});

app.listen(PORT, () => {
    console.log(`Server is listening on ${PORT} port`);
});
