const app = require('./src/app');
const request = require("supertest");
const User = require("./src/models/user");

describe("User Registration Validation Error Test", () => {
    beforeEach(async () => {
        await User.deleteMany();
    });

    it("should return status 401 and error message for validation error", (done) => {
        request(app)
            .post("/register")
            .send({
                name: "User1",
                email: "invalidemail",
                password: "password123",
                age: 25
            })
            .expect(401)
            .then(response => {
                expect(response.body.error).toBe("Validation error");
                done(); // Для позначення завершення тесту
            })
            .catch(error => done(error)); // Обробляє помилки тесту
    });

});