const express = require("express");
const User = require("../models/user");
const auth = require('../auth');

const router = express.Router();

// Маршрутизатор для отримання всіх користувачів
router.get("/users", auth, async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Маршрутизатор для отримання користувача за його ID
router.get("/users/:id", auth, async (req, res) => {
    try {
        const user = await User.findById(req.params.id).populate("tasks");
        if (!user) {
            return res.status(404).send("User not found");
        }
        res.json(user);
    } catch (error) {
        res.status(500).send(error.message);
    }
});


// Маршрутизатор для створення нового користувача
router.post("/users", async (req, res) => {
    try {
        const user = new User(req.body);
        await user.save();
        res.status(201).json(user);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрутизатор для оновлення користувача за його ID
router.put("/users/:id", auth, async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!user) {
            return res.status(404).send("User not found");
        }
        res.json(user);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрутизатор для видалення користувача за його ID
router.delete("/users/:id", auth, async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        if (!user) {
            return res.status(404).send("User not found");
        }
        res.status(200).json(user);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрутизатор для видалення всіх користувачів
router.delete("/users", auth, async (req, res) => {
    try {
        const result = await User.deleteMany();
        res.status(200).json(result);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// PATCH запит для редагування даних користувача за його id
router.patch('/users/:id', auth, async (req, res) => {
    const userId = req.params.id;

    try {
        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }

        const fields = ["name", "email", "password", "age"];
        fields.forEach((field) => {
            if (req.body[field]) {
                user[field] = req.body[field];
            }
        })

        await user.save();

        return res.status(200).json(user);
    } catch (error) {
        console.error('Error updating user:', error);
        return res.status(500).json({ error: 'Internal Server Error' });
    }
});

// POST запит для авторизації користувача
router.post('/users/login', async (req, res) => {
    const { email, password } = req.body;

    try {
        const user = await User.findOneByCredentials(email, password);
        const token = await user.generateAuthToken();
        res.send({user, token});
    } catch (error) {
        console.error('Authentication error:', error.message);
        return res.status(400).json({ error: 'Invalid credentials' });
    }
});

// GET запит для отримання даних користувача
router.get('/users/me', auth, async (req, res) => {
    try {
        // Отримуємо користувача з middleware auth
        const user = req.user;
        await user.populate("tasks").execPopulate();

        res.send(user); // Повертаємо користувача у відповідь
    } catch (error) {
        console.error(error);
        res.status(500).send({ error: 'Server Error' });
    }
});


// POST запит для виходу користувача
router.post('/users/logout', auth, async (req, res) => {
    try {
        // Отримуємо поточені токени користувача
        req.user.tokens = req.user.tokens.filter((token) => token.token !== req.token);

        // Зберігаємо зміни у базі даних
        await req.user.save();

        res.status(200).send('Logout successful');
    } catch (error) {
        console.error('Error:', error.message);
        res.status(500).send('Server Error');
    }
});

module.exports = router;
