const express = require("express");
const Task = require("../models/task");
const auth = require('../auth');

const router = express.Router();

// Маршрутизатор для отримання всіх задач
router.get("/tasks", auth, async (req, res) => {
    try {
        const tasks = await Task.find({ owner: req.user.id });
        res.json(tasks);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Маршрутизатор для отримання задачі за її ID
router.get("/tasks/:id", auth, async (req, res) => {
    try {
        const task = await Task.findById(req.params.id);
        await task.populate('owner');
        if (!task) {
            return res.status(404).send("Task not found");
        }
        res.json(task);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Маршрутизатор для отримання задачі за її ID, лише для власника задачі
router.get("/tasks2/:id", auth, async (req, res) => {
    try {
        const task = await Task.findOne({ _id: req.params.id, owner: req.user.id });
        if (!task) {
            return res.status(404).send("Task not found");
        }
        res.json(task);
    } catch (error) {
        res.status(500).send(error.message);
    }
});


// Маршрутизатор для створення нової задачі
router.post("/tasks", auth, async (req, res) => {
    const task = new Task({
        ...req.body,
        owner: req.user.id
    });
    try {
        await task.save();
        await task.populate('owner'); // Отримання запису користувача
        res.status(201).json(task);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Маршрутизатор для оновлення задачі за її ID
router.put("/tasks/:id", auth, async (req, res) => {
    const updates = Object.keys(req.body);

    try {
        const task = await Task.findOneAndUpdate({ _id: req.params.id, owner: req.user.id }, req.body, { new: true });

        if (!task) {
            return res.status(404).send("Task not found");
        }

        res.json(task);
    } catch (error) {
        res.status(500).send(error.message);
    }
});


// Маршрутизатор для видалення задачі за її ID
router.delete("/tasks/:id", auth, async (req, res) => {
    try {
        const task = await Task.findOneAndDelete({ _id: req.params.id, owner: req.user.id });

        if (!task) {
            return res.status(404).send("Task not found");
        }

        res.status(200).json(task);
    } catch (error) {
        res.status(500).send(error.message);
    }
});


// Маршрутизатор для відмітки задачі як виконаної
router.patch("/tasks/:id/completed", auth, async (req, res) => {
    try {
        const task = await Task.findByIdAndUpdate(req.params.id, { completed: true }, { new: true });
        if (!task) {
            return res.status(404).send("Task not found");
        }
        res.status(200).json(task);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

module.exports = router;