const jwt = require('jsonwebtoken');
const User = require('./models/user');

const auth = async (req, res, next) => {
    try {
        // Токен з заголовка Authorization
        const token = req.header('Authorization').replace('Bearer ', '');


        // Декодує токен та знаходить користувача за його id
        const decoded = jwt.verify(token, 'kdweueksdsjfij');
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token });

        if (!user) {
            throw new Error();
        }

        // Записує користувача в об'єкт req для подальшого використання
        req.user = user;
        req.token = token;
        next();
    } catch (error) {
        res.status(401).send({ error: 'Please authenticate' });
    }
};

module.exports = auth;
